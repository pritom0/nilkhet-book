package com.allandroidprojects.ecomsample.utility;

import java.util.ArrayList;

/**
 * Created by 06peng on 2015/6/24.
 */
public class ImageUrlUtils {
    static ArrayList<String> wishlistImageUri = new ArrayList<>();
    static ArrayList<String> cartListImageUri = new ArrayList<>();

    public static String[] getImageUrls() {
        String[] urls = new String[] {
                "https://images-na.ssl-images-amazon.com/images/I/51lr5T3UQ8L._SX322_BO1,204,203,200_.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Business%20Advice37753.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/gaanbabu74572.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/XYZ40751.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/gaanbabu74572.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Business%20Advice37753.jpg",

                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",

                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",

                "https://static.pexels.com/photos/7093/coffee-desk-notes-workspace-medium.jpg",
        };
        return urls;
    }

    public static String[] getOffersUrls() {
        String[] urls = new String[]{


                "http://gyannbabu.ennvisiodigital.tech/images/products/Business%20Advice37753.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/gaanbabu74572.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/XYZ40751.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/gaanbabu74572.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Business%20Advice37753.jpg",
                "http://4.bp.blogspot.com/-3kana3t03NI/U1qlB6NYTOI/AAAAAAAAE9g/h_vSp_myTq8/s320/Ma+By+Anisul+Haque.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",

                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg"

        };
        return urls;
    }

    public static String[] getHomeApplianceUrls() {
        String[] urls = new String[]{
                "https://images-na.ssl-images-amazon.com/images/I/51lr5T3UQ8L._SX322_BO1,204,203,200_.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Business%20Advice37753.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/gaanbabu74572.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/XYZ40751.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/gaanbabu74572.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Business%20Advice37753.jpg",
                 "https://upload.wikimedia.org/wikipedia/en/thumb/f/f4/Cover_of_the_Book_Deshe_Bideshe_by_Syed_Mujtaba_Ali.jpg/220px-Cover_of_the_Book_Deshe_Bideshe_by_Syed_Mujtaba_Ali.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",
                "https://images-na.ssl-images-amazon.com/images/I/41GJVyhZM6L._SX331_BO1,204,203,200_.jpg",
                "https://images-na.ssl-images-amazon.com/images/I/41GJVyhZM6L._SX331_BO1,204,203,200_.jpg",

                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",

        };
        return urls;
    }

    public static String[] getElectronicsUrls() {
        String[] urls = new String[]{
                "https://images-na.ssl-images-amazon.com/images/I/51lr5T3UQ8L._SX322_BO1,204,203,200_.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Business%20Advice37753.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/gaanbabu74572.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/XYZ40751.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/gaanbabu74572.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Business%20Advice37753.jpg",
                "https://static.pexels.com/photos/211048/pexels-photo-211048-medium.jpeg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",

                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",

        };
        return urls;
    }

    public static String[] getLifeStyleUrls() {
        String[] urls = new String[]{
                "https://images-na.ssl-images-amazon.com/images/I/51lr5T3UQ8L._SX322_BO1,204,203,200_.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Business%20Advice37753.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/gaanbabu74572.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/XYZ40751.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/gaanbabu74572.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Business%20Advice37753.jpg",
                "https://static.pexels.com/photos/211048/pexels-photo-211048-medium.jpeg",
                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",

                "http://gyannbabu.ennvisiodigital.tech/images/products/Adobe%20Design35806.jpg",

        };
        return urls;
    }

    public static String[] getBooksUrls() {
        String[] urls = new String[]{
                "https://static.pexels.com/photos/67442/pexels-photo-67442-medium.jpeg",
                "https://static.pexels.com/photos/159494/book-glasses-read-study-159494-medium.jpeg",
                "https://static.pexels.com/photos/33283/stack-of-books-vintage-books-book-books-medium.jpg",
                "https://static.pexels.com/photos/205323/pexels-photo-205323-medium.png",
                "https://static.pexels.com/photos/38167/pexels-photo-38167-medium.jpeg",
                "https://static.pexels.com/photos/68562/pexels-photo-68562-medium.jpeg",
                "https://static.pexels.com/photos/34592/pexels-photo-medium.jpg",
                "https://static.pexels.com/photos/1579/hand-notes-holding-things-medium.jpg",
                "https://static.pexels.com/photos/26890/pexels-photo-26890-medium.jpg",
                "https://static.pexels.com/photos/67442/pexels-photo-67442-medium.jpeg",
                "https://static.pexels.com/photos/159494/book-glasses-read-study-159494-medium.jpeg",
                "https://static.pexels.com/photos/33283/stack-of-books-vintage-books-book-books-medium.jpg",
                "https://static.pexels.com/photos/205323/pexels-photo-205323-medium.png",
                "https://static.pexels.com/photos/38167/pexels-photo-38167-medium.jpeg",
                "https://static.pexels.com/photos/68562/pexels-photo-68562-medium.jpeg",
        };
        return urls;
    }

    // Methods for Wishlist
    public void addWishlistImageUri(String wishlistImageUri) {
        this.wishlistImageUri.add(0,wishlistImageUri);
    }

    public void removeWishlistImageUri(int position) {
        this.wishlistImageUri.remove(position);
    }

    public ArrayList<String> getWishlistImageUri(){ return this.wishlistImageUri; }

    // Methods for Cart
    public void addCartListImageUri(String wishlistImageUri) {
        this.cartListImageUri.add(0,wishlistImageUri);
    }

    public void removeCartListImageUri(int position) {
        this.cartListImageUri.remove(position);
    }

    public ArrayList<String> getCartListImageUri(){ return this.cartListImageUri; }
}
